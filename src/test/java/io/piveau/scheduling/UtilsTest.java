package io.piveau.scheduling;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

class UtilsTest {

    @Test
    @DisplayName("Test URL generation")
    void testURLGeneration() {
        System.out.println(Utils.generateDashboardURL(Instant.now().minus(1, ChronoUnit.HOURS), Instant.now(), "european-union-open-data-portal"));
    }

}
