package io.piveau.scheduling;

import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.TriggerBuilder.newTrigger;

@DisplayName("Testing Quartz Scheduler")
@ExtendWith(VertxExtension.class)
@Disabled
class QuartzTest {

    private Scheduler scheduler;

    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            testContext.completeNow();
        } catch (SchedulerException e) {
            testContext.failNow(e);
        }
    }

    @Test
    @DisplayName("Test cron syntax")
    void testCron(Vertx vertx, VertxTestContext testContext) {

        String cron = " 0 12 * * * ? ";
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cron);
        TriggerBuilder<CronTrigger> builder = newTrigger()
                .withIdentity("test", "test")
                .withSchedule(scheduleBuilder.withMisfireHandlingInstructionDoNothing());

        CronTrigger trigger = builder.build();

        testContext.verify(() -> {
            Assertions.assertNotNull(trigger);
        });

        testContext.completeNow();
    }

}
