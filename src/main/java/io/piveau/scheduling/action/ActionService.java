package io.piveau.scheduling.action;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.tracing.TracingPolicy;

@ProxyGen
public interface ActionService {
    String SERVICE_ADDRESS = "io.piveau.scheduling.action.service";

    static ActionService create(Vertx vertx, Handler<AsyncResult<ActionService>> readyHandler) {
        return new ActionServiceImpl(vertx, readyHandler);
    }

    static ActionService createProxy(Vertx vertx, String address) {
        return new ActionServiceVertxEBProxy(vertx, address, new DeliveryOptions().setTracingPolicy(TracingPolicy.PROPAGATE));
    }

    Future<JsonObject> runPipe(JsonObject params);

}
