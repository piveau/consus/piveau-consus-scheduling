package io.piveau.scheduling.action;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.serviceproxy.ServiceBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActionServiceVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void start(Promise<Void> startPromise) {
        log.debug("Start Action Service Verticle");
        ActionService.create(vertx, ready -> {
            if (ready.succeeded()) {
                new ServiceBinder(vertx).setAddress(ActionService.SERVICE_ADDRESS).register(ActionService.class, ready.result());
                startPromise.complete();
            } else {
                startPromise.fail(ready.cause());
            }
        });
    }

}
