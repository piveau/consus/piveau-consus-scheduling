package io.piveau.scheduling.action;

import io.piveau.scheduling.launcher.LauncherService;
import io.piveau.telemetry.PiveauTelemetry;
import io.piveau.telemetry.PiveauTelemetryTracer;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class ActionServiceImpl implements ActionService {

    private final PiveauTelemetryTracer tracer = PiveauTelemetry.getTracer("piveau-consus-scheduling");

    private final LauncherService launcherService;

    ActionServiceImpl(Vertx vertx, Handler<AsyncResult<ActionService>> readyHandler) {
        launcherService = LauncherService.createProxy(vertx, LauncherService.SERVICE_ADDRESS);
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public Future<JsonObject> runPipe(JsonObject params) {
        String pipeName = params.getString("pipeName", "");
        JsonObject configs = params.getJsonObject("configs", new JsonObject());

        // Needs just to be forwarded ;-)
        return launcherService.launch(pipeName, configs);
    }

}
