package io.piveau.scheduling;

public class Defaults {
    private Defaults() {}

    static final int DEFAULT_APPLICATION_PORT = 8080;

    static final String DEFAULT_PIVEAU_FAVICON_PATH = "webroot/images/favicon.png";
    static final String DEFAULT_PIVEAU_LOGO_PATH = "webroot/images/logo.png";

}
