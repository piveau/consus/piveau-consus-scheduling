package io.piveau.scheduling;

public final class Constants {

    private Constants() {}

    static final String ENV_APPLICATION_PORT = "PORT";

    static final String ENV_PIVEAU_FAVICON_PATH = "PIVEAU_FAVICON_PATH";

    static final String ENV_PIVEAU_LOGO_PATH = "PIVEAU_LOGO_PATH";

    public static final String ENV_PIVEAU_CLUSTER_CONFIG = "PIVEAU_CLUSTER_CONFIG";
    public static final String ENV_PIVEAU_SHELL_CONFIG = "PIVEAU_SHELL_CONFIG";

    public static final String ENV_PIVEAU_DASHBOARD_LOGS_URL_TEMPLATE = "PIVEAU_DASHBOARD_LOGS_URL_TEMPLATE";
    public static final String ENV_PIVEAU_PORTAL_URL = "PIVEAU_PORTAL_URL";

    public static final String ACTIVE = "active";
    public static final String FINISHED = "finished";
    public static final String CANCELED = "canceled";
    public static final String FAILED = "failed";

    public static final String AUTH_FILE = "security/auth.properties";
}
