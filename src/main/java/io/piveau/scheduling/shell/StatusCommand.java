package io.piveau.scheduling.shell;

import io.piveau.scheduling.launcher.LauncherService;
import io.vertx.core.Vertx;
import io.vertx.core.cli.CLI;
import io.vertx.core.cli.Option;
import io.vertx.ext.shell.command.Command;
import io.vertx.ext.shell.command.CommandBuilder;

import java.util.List;
import java.util.Set;

public class StatusCommand {
    private final Command command;

    private final LauncherService launcherService;

    private StatusCommand(Vertx vertx) {
        launcherService = LauncherService.createProxy(vertx, LauncherService.SERVICE_ADDRESS);
        command = CommandBuilder.command(
                CLI.create("status")
                        .setDescription("List statuses of all pipe runs, or show the status of a specific run")
                        .addOption(new Option()
                                .setArgName("runId")
                                .setLongName("runId")
                                .setShortName("r")
                                .setDescription("Get status of a specific runId. Any filter is ignored")
                                .setMultiValued(false)
                        )
                        .addOption(new Option()
                                .setArgName("filterStatus")
                                .setChoices(Set.of("active", "canceled", "finished", "failed"))
                                .setMultiValued(true)
                                .setDescription("Only list runs with specific statuses.")
                                .setLongName("filterStatus").setShortName("fs"))
                        .addOption(new Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help"))
        ).processHandler(process -> {
            List<String> filter = process.commandLine().getOptionValues("filterStatus");
            String runId = process.commandLine().getOptionValue("runId");
            if (runId != null && !filter.isEmpty()) {
                process.write("Filter is ignored when runId is used\n");
            }

            if (runId != null) {
                launcherService.status(runId)
                        .onSuccess(status -> process.write("\n" + status.encodePrettily() + "\n").end())
                        .onFailure(cause -> process.write(cause.getMessage() + "\n").end());
            } else {
                launcherService.list(filter)
                        .onSuccess(list -> process.write("\n" + list.encodePrettily() + "\n").end())
                        .onFailure(cause -> process.write(cause.getMessage() + "\n").end());
            }
        }).build(vertx);
    }

    public static Command create(Vertx vertx) {
        return new StatusCommand(vertx).command;
    }

}
