package io.piveau.scheduling.shell;

import io.piveau.scheduling.launcher.LauncherService;
import io.vertx.core.Vertx;
import io.vertx.core.cli.Argument;
import io.vertx.core.cli.CLI;
import io.vertx.core.cli.Option;
import io.vertx.ext.shell.command.Command;
import io.vertx.ext.shell.command.CommandBuilder;

public class CancelCommand {
    private final Command command;

    private final LauncherService launcherService;

    private CancelCommand(Vertx vertx) {
        launcherService = LauncherService.createProxy(vertx, LauncherService.SERVICE_ADDRESS);
        command = CommandBuilder.command(
                CLI.create("cancel")
                        .setDescription("Cancel a pipe run")
                        .addArgument(
                                new Argument().setIndex(1)
                                        .setArgName("runId")
                                        .setRequired(true)
                                        .setDescription("Id of the run"))
                        .addOption(new Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help"))
        ).processHandler(process -> {
            String runId = process.commandLine().getArgumentValue(0);
            if (runId != null) {
                launcherService.cancel(runId)
                        .onSuccess(status -> process.write("Pipe " + status.getString("pipeName", "?") + " successfully canceled.\n").end())
                        .onFailure(cause -> process.write(cause.getMessage() + "\n").end());
            } else {
                process.write("Please, provide a run id.").end();
            }
        }).build(vertx);
    }

    public static Command create(Vertx vertx) {
        return new CancelCommand(vertx).command;
    }

}
