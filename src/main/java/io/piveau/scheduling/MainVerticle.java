package io.piveau.scheduling;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.api.trace.StatusCode;
import io.opentelemetry.context.Scope;
import io.piveau.scheduling.action.ActionServiceVerticle;
import io.piveau.scheduling.dashboard.Dashboard;
import io.piveau.scheduling.jsonrpc.JSONRPC;
import io.piveau.scheduling.launcher.LauncherService;
import io.piveau.scheduling.launcher.LauncherServiceVerticle;
import io.piveau.scheduling.quartz.QuartzService;
import io.piveau.scheduling.quartz.QuartzServiceVerticle;
import io.piveau.scheduling.shell.ShellVerticle;
import io.piveau.telemetry.PiveauTelemetry;
import io.piveau.telemetry.PiveauTelemetryTracer;
import io.piveau.utils.ConfigurableAssetHandler;
import io.piveau.vertx.PiveauVertxLauncher;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.tracing.TracingPolicy;
import io.vertx.ext.auth.authorization.PermissionBasedAuthorization;
import io.vertx.ext.auth.properties.PropertyFileAuthentication;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.auth.properties.PropertyFileAuthorization;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.AuthorizationHandler;
import io.vertx.ext.web.handler.BasicAuthHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.router.RouterBuilder;
import io.vertx.openapi.contract.OpenAPIContract;
import io.vertx.openapi.validation.RequestParameter;
import io.vertx.openapi.validation.SchemaValidationException;
import io.vertx.openapi.validation.ValidatedRequest;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static io.piveau.scheduling.Constants.*;
import static io.piveau.scheduling.Defaults.*;

public class MainVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private QuartzService quartzService;

    private LauncherService launcherService;

    private JSONRPC jsonrpc;

    private final PiveauTelemetryTracer tracer = PiveauTelemetry.getTracer("piveau-consus-scheduling");

    private Dashboard dashboard;

    @Override
    public void start(Promise<Void> startPromise) {

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add(ENV_APPLICATION_PORT)
                        .add(ENV_PIVEAU_CLUSTER_CONFIG)
                        .add(ENV_PIVEAU_SHELL_CONFIG)
                        .add(ENV_PIVEAU_FAVICON_PATH)
                        .add(ENV_PIVEAU_LOGO_PATH)
                        .add(ENV_PIVEAU_DASHBOARD_LOGS_URL_TEMPLATE)
                        .add(ENV_PIVEAU_PORTAL_URL)));

        ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
                .setType("directory")
                .setConfig(new JsonObject()
                        .put("path", "config")
                        .put("filesets", new JsonArray()
                                .add(new JsonObject()
                                        .put("format", "json")
                                        .put("pattern", "*.json"))
                                .add(new JsonObject()
                                        .put("format", "yaml")
                                        .put("pattern", "*.yaml"))
                                .add(new JsonObject()
                                        .put("format", "yaml")
                                        .put("pattern", "*.yml"))
                                .add(new JsonObject()
                                        .put("format", "properties")
                                        .put("pattern", "*.properties"))));

        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions()
                .addStore(envStoreOptions)
                .addStore(fileStoreOptions));

        AtomicReference<String> logoPath = new AtomicReference<>();
        AtomicReference<String> faviconPath = new AtomicReference<>();

        AtomicInteger port = new AtomicInteger();

        retriever.getConfig()
                .compose(config -> {
                    log.info(config.encodePrettily());

                    faviconPath.set(config.getString(ENV_PIVEAU_FAVICON_PATH, DEFAULT_PIVEAU_FAVICON_PATH));
                    logoPath.set(config.getString(ENV_PIVEAU_LOGO_PATH, DEFAULT_PIVEAU_LOGO_PATH));

                    port.set(config.getInteger(ENV_APPLICATION_PORT, DEFAULT_APPLICATION_PORT));

                    return vertx.deployVerticle(LauncherServiceVerticle.class, new DeploymentOptions()
                                    .setThreadingModel(ThreadingModel.WORKER)
                                    .setConfig(config))
                            .map(config);
                })
                .compose(config -> {
                    Future<String> actionFuture = vertx.deployVerticle(ActionServiceVerticle.class, new DeploymentOptions()
                            .setThreadingModel(ThreadingModel.WORKER)
                            .setConfig(config));
                    Future<String> quartzFuture = vertx.deployVerticle(QuartzServiceVerticle.class, new DeploymentOptions()
                            .setThreadingModel(ThreadingModel.WORKER)
                            .setConfig(config));
                    Future<String> shellFuture = vertx.deployVerticle(ShellVerticle.class, new DeploymentOptions()
                            .setThreadingModel(ThreadingModel.WORKER)
                            .setConfig(config));

                    return Future.all(actionFuture, quartzFuture, shellFuture).map(config);
                })
                .compose(config -> {
                    quartzService = QuartzService.createProxy(vertx, QuartzService.SERVICE_ADDRESS);
                    launcherService = LauncherService.createProxy(vertx, LauncherService.SERVICE_ADDRESS);

                    Utils.dashboardLogsUrlTemplate = config.getString(ENV_PIVEAU_DASHBOARD_LOGS_URL_TEMPLATE, "");

                    dashboard = new Dashboard(
                            vertx,
                            quartzService,
                            launcherService,
                            config.getString(ENV_PIVEAU_PORTAL_URL, ""));

                    jsonrpc = new JSONRPC(vertx);

                    return OpenAPIContract.from(vertx, "webroot/openapi.yaml");
                })
                .onSuccess(contract -> {

                    RouterBuilder builder = RouterBuilder.create(vertx, contract);

//                    builder.rootHandler(CorsHandler.create().addRelativeOrigin("*").allowedHeader("Content-Type").allowedMethods(Stream.of(HttpMethod.PUT, HttpMethod.GET).collect(Collectors.toSet())));

                    builder.rootHandler(StaticHandler.create());

                    builder.getRoute("listPipes").addHandler(this::handleListPipes);
                    builder.getRoute("getPipe").addHandler(this::handleGetPipe);
                    builder.getRoute("putPipe")
//                            .addHandler(new PermissionHandler(List.of(
//                                    Constants.AUTH_SCOPE_PIPE_CREATE,
//                                    Constants.AUTH_SCOPE_PIPE_UPDATE)))
                            .addHandler(this::handlePutPipe);

                    builder.getRoute("listRuns").addHandler(this::handleListRuns);
                    builder.getRoute("getRun").addHandler(this::handleGetRun);
                    builder.getRoute("deleteRun").addHandler(this::handleDeleteRun);

                    builder.getRoute("listTriggers").addHandler(this::handleListTriggers);
                    builder.getRoute("deleteTriggers").addHandler(this::handleDeleteTriggers);
                    builder.getRoute("getTrigger").addHandler(this::handleGetTrigger);
                    builder.getRoute("putTrigger").addHandler(this::handlePutTrigger);
                    builder.getRoute("patchTrigger").addHandler(this::handlePatchTrigger);
                    builder.getRoute("deleteTrigger").addHandler(this::handleDeleteTrigger);

                    builder.getRoute("mapTriggers").addHandler(this::handleMapTriggers);
                    builder.getRoute("putTriggers").addHandler(this::handlePutTriggers);

                    builder.getRoute("postAction").addHandler(this::handlePostAction);

                    Router router = builder.createRouter();

                    Route dashboardRoute = router.get("/dashboard");
                    if (vertx.fileSystem().existsBlocking(AUTH_FILE)) {
                        log.info("Dashboard authentication properties found.");
                        dashboardRoute.handler(BasicAuthHandler.create(PropertyFileAuthentication.create(vertx, AUTH_FILE)));
                        dashboardRoute.handler(AuthorizationHandler.create(
                                        PermissionBasedAuthorization.create("view")
                                )
                                .addAuthorizationProvider(PropertyFileAuthorization.create(vertx, AUTH_FILE)));
                    }
                    dashboardRoute.handler(dashboard::handleGetDashboard);

                    router.errorHandler(400, routingContext -> {
                        if (routingContext.failure() instanceof SchemaValidationException sve) {
                            routingContext.response().setStatusCode(400).end(sve.getMessage());
                        }
                    });

                    WebClient client = WebClient.create(vertx);
                    router.route("/images/logo").handler(new ConfigurableAssetHandler(logoPath.get(), client));
                    router.route("/images/favicon").handler(new ConfigurableAssetHandler(faviconPath.get(), client));

                    vertx.fileSystem().readFile("buildInfo.json")
                            .onSuccess(buffer -> {
                                HealthCheckHandler hch = HealthCheckHandler.create(vertx);
                                hch.register("buildInfo", pr -> pr.complete(Status.OK(buffer.toJsonObject())));
                                router.get("/health").handler(hch);
                            });

                    vertx.createHttpServer(new HttpServerOptions().setTracingPolicy(TracingPolicy.PROPAGATE))
                            .requestHandler(router)
                            .listen(port.get())
                            .onSuccess(success -> startPromise.complete())
                            .onFailure(startPromise::fail);
                })
                .onFailure(startPromise::fail);
    }

    private void handleListPipes(RoutingContext routingContext) {

        Span span = tracer.span("listPipes", SpanKind.SERVER);

        Scope scope = span.makeCurrent();
        launcherService.availablePipeNames()
                .onSuccess(list -> {
                    span.addEvent("Success", Attributes.of(AttributeKey.stringArrayKey("list"), list));
                    routingContext.json(new JsonArray(list));
                })
                .onFailure(cause -> {
                    span.recordException(cause);
                    span.setStatus(StatusCode.ERROR);
                    serviceException(cause, routingContext);
                })
                .onComplete(ar -> {
                    scope.close();
                    span.end();
                });
    }

    private void handleGetPipe(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        launcherService.getPipe(pipeName)
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePutPipe(RoutingContext routingContext) {

        if (routingContext.fileUploads().size() != 1) {
            routingContext.response().setStatusCode(400).end("Only one file allowed in upload");
            return;
        }

        FileUpload fileUpload = routingContext.fileUploads().get(0);

        launcherService.putPipe(fileUpload.uploadedFileName(), fileUpload.fileName())
                .onSuccess(result -> {
                    switch (result.getString("status", "")) {
                        case "added" -> routingContext.end();
                        case "created" -> routingContext.response().setStatusCode(201).end();
                        case "updated" -> routingContext.response().setStatusCode(204).end();
                        default -> routingContext.response().setStatusCode(500).end();
                    }
                })
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleListRuns(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        RequestParameter filterQuery = validatedRequest.getQuery().get("filter");

        List<String> filter = filterQuery.getJsonArray(new JsonArray()).stream().map(String.class::cast).toList();
        launcherService.list(filter)
                .onSuccess(array -> routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(array.encodePrettily()))
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleGetRun(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String runId = validatedRequest.getPathParameters().get("runId").getString();

        launcherService.status(runId)
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleDeleteRun(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String runId = validatedRequest.getPathParameters().get("runId").getString();

        launcherService.cancel(runId)
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleListTriggers(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        quartzService.getTriggers(pipeName)
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleDeleteTriggers(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        quartzService.deleteTriggers(pipeName)
                .onSuccess(v -> routingContext.response().setStatusCode(204).end())
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleGetTrigger(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        String triggerId = validatedRequest.getPathParameters().get("triggerId").getString();
        quartzService.getTrigger(pipeName, triggerId)
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePutTrigger(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        String triggerId = validatedRequest.getPathParameters().get("triggerId").getString();
        JsonObject trigger = validatedRequest.getBody().getJsonObject();
        quartzService.putTrigger(pipeName, triggerId, trigger)
                .onSuccess(statusCode -> {
                    int code = "created".equalsIgnoreCase(statusCode) ? 201 : 200;
                    routingContext.response().setStatusCode(code).end();
                })
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleDeleteTrigger(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        String triggerId = validatedRequest.getPathParameters().get("triggerId").getString();
        quartzService.deleteTrigger(pipeName, triggerId)
                .onSuccess(v -> routingContext.response().setStatusCode(204).end())
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePatchTrigger(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        String pipeName = validatedRequest.getPathParameters().get("pipeName").getString();
        String triggerId = validatedRequest.getPathParameters().get("triggerId").getString();
        JsonObject patch = validatedRequest.getBody().getJsonObject();

        quartzService.patchTrigger(pipeName, triggerId, patch)
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleMapTriggers(RoutingContext routingContext) {
        quartzService.listTriggers()
                .onSuccess(routingContext::json)
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePutTriggers(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        boolean resetPipe = validatedRequest.getQuery().get("resetPipes").getBoolean(false);
        JsonObject bulk = validatedRequest.getBody().getJsonObject();

        Map<String, List<JsonObject>> map = bulk.stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e ->
                        ((JsonArray) e.getValue()).stream().map(JsonObject.class::cast).toList()
                ));

        // reset
        List<Future<Void>> resetFutures = map.keySet().stream()
                .map(pipeName ->
                        resetPipe ?
                                quartzService.deleteTriggers(pipeName).otherwiseEmpty()
                                : Future.<Void>succeededFuture()
                )
                .toList();

        Future.join(resetFutures)
                .transform(ar -> {
                    List<Future<String>> futures = map.entrySet().stream()
                            .flatMap(e -> e.getValue().stream().map(t -> Map.entry(e.getKey(), t)))
                            .map(e -> quartzService.putTrigger(e.getKey(), e.getValue().getString("id"), e.getValue()))
                            .toList();
                    return Future.join(futures);
                })
                .onComplete(ar -> {
                    List<String> errors = ar.result().causes().stream()
                            .map(Throwable::getMessage).toList();
                    routingContext.json(new JsonArray(errors));
                });
    }

    private void handlePostAction(RoutingContext routingContext) {
        ValidatedRequest validatedRequest = routingContext.get(RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST);
        JsonObject jsonrpcRequest = validatedRequest.getBody().getJsonObject();
        jsonrpc.dispatch(jsonrpcRequest)
                .onSuccess(result -> {
                    if (result.isEmpty()) {
                        routingContext.response().setStatusCode(202).end();
                    } else {
                        routingContext.json(result);
                    }
                })
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void serviceException(Throwable cause, RoutingContext routingContext) {
        if (cause instanceof ServiceException se) {
            routingContext.fail(se.failureCode(), se);
        } else {
            routingContext.fail(cause);
        }
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 2);
        params[params.length - 2] = "run";
        params[params.length - 1] = MainVerticle.class.getName();

        PiveauVertxLauncher launcher = new PiveauVertxLauncher();
        launcher.dispatch(params);
    }

}
