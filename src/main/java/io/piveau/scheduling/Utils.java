package io.piveau.scheduling;

import java.time.Instant;

public class Utils {
    private Utils() {
    }

    public static String dashboardLogsUrlTemplate = "";

    public static String generateDashboardURL(Instant from, Instant to, String pipeName) {
        return String.format(dashboardLogsUrlTemplate, from.toString(), to.toString(), pipeName);
    }

    public static String extractPipeName(String runId) {
        if (runId.contains("~")) {
            return runId.substring(0, runId.indexOf("~"));
        } else {
            return "";
        }
    }

}
