package io.piveau.scheduling.jsonrpc;

import io.piveau.scheduling.action.ActionService;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;

public class JSONRPC {

    private static final String JSONRPC_PROPERTY = "jsonrpc";
    private static final String JSONRPC_VERSION = "2.0";

    ActionService actionService;

    public JSONRPC(Vertx vertx) {
        actionService = ActionService.createProxy(vertx, ActionService.SERVICE_ADDRESS);
    }

    public Future<JsonObject> dispatch(JsonObject request) {
        Method method = Method.valueOf(request.getString("method").toUpperCase());
        JsonObject params = request.getJsonObject("params", new JsonObject());

        switch (method) {
            case RUNPIPE -> {
                return actionService.runPipe(params)
                        .map(result -> createSuccessResponse(request, result))
                        .recover(cause -> Future.succeededFuture(createErrorResponse(request, cause)));
            }
            case DUMMY -> {
                return Future.failedFuture(new ServiceException(500, ""));
            }
            default -> {
                return Future.failedFuture(new ServiceException(403, ""));
            }
        }
    }

    private JsonObject createSuccessResponse(JsonObject request, JsonObject result) {
        return new JsonObject()
                .put(JSONRPC_PROPERTY, JSONRPC_VERSION)
                .put("result", result)
                .put("id", request.getString("id"));
    }

    private JsonObject createErrorResponse(JsonObject request, Throwable cause) {
        return new JsonObject()
                .put(JSONRPC_PROPERTY, JSONRPC_VERSION)
                .put("error", new JsonObject()
                        .put("code", -9)
                        .put("message", cause.getMessage()))
                .put("id", request.getString("id"));
    }

}
