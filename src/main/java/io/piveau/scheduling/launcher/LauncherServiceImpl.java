package io.piveau.scheduling.launcher;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.StatusCode;
import io.piveau.pipe.PipeLauncher;
import io.piveau.pipe.PiveauCluster;
import io.piveau.pipe.model.ModelKt;
import io.piveau.scheduling.Utils;
import io.piveau.telemetry.PiveauTelemetry;
import io.piveau.telemetry.PiveauTelemetryTracer;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class LauncherServiceImpl implements LauncherService {

    private static final Logger log = LoggerFactory.getLogger(LauncherServiceImpl.class);
    private PipeLauncher launcher;

    private final PiveauTelemetryTracer tracer = PiveauTelemetry.getTracer("piveau-consus-scheduling");

    LauncherServiceImpl(Vertx vertx, PiveauCluster cluster, Handler<AsyncResult<LauncherService>> readyHandler) {
        cluster.pipeLauncher(vertx)
                .onSuccess(pipeLauncher -> {
                    this.launcher = pipeLauncher;
                    readyHandler.handle(Future.succeededFuture(this));
                })
                .onFailure(t -> readyHandler.handle(Future.failedFuture(t)));
    }

    @Override
    public Future<JsonObject> launch(String pipeName, JsonObject configs) {
        if (launcher.isPipeAvailable(pipeName)) {
            return launcher.runPipe(pipeName, configs, null);
        } else {
            return Future.failedFuture(new ServiceException(404, "Pipe not found"));
        }
    }

    @Override
    public Future<JsonObject> status(String runId) {
        String pipeName = Utils.extractPipeName(runId);
        if (pipeName.isBlank()) {
            return Future.failedFuture(new ServiceException(501, "Inappropriate run id"));
        } else {
            return launcher.getRunStatus(pipeName, runId);
        }
    }

    @Override
    public Future<JsonObject> cancel(String runId) {
        String pipeName = Utils.extractPipeName(runId);
        if (pipeName.isBlank()) {
            return Future.failedFuture(new ServiceException(501, "Inappropriate run id"));
        } else {
            return launcher.cancelRun(pipeName, runId).map(new JsonObject());
        }
    }

    @Override
    public Future<JsonArray> list(List<String> filter) {
        return launcher.listRuns()
                .map(list -> {
                    log.debug(list.toString());
                    return list.stream()
                            .filter(statusInfo -> filter.isEmpty()
                                    || filter.contains(statusInfo.getString("status", "")))
                            .toList();
                })
                .map(JsonArray::new);
    }

    @Override
    public Future<Boolean> isPipeAvailable(String pipeName) {
        return Future.succeededFuture(launcher.isPipeAvailable(pipeName));
    }

    @Override
    public Future<JsonObject> getPipe(String pipeName) {
        if (launcher.isPipeAvailable(pipeName)) {
            return Future.succeededFuture(new JsonObject(ModelKt.prettyPrint(launcher.getPipe(pipeName))));
        } else {
            return Future.failedFuture(new ServiceException(404, "Pipe not found"));
        }
    }

    @Override
    public Future<JsonObject> putPipe(String uploadedFilename, String filename) {
        return launcher.addPipe(uploadedFilename, filename);
    }

    @Override
    public Future<List<JsonObject>> availablePipes() {
        try {
            List<JsonObject> list = launcher.availablePipes().stream().map(p -> new JsonObject(ModelKt.prettyPrint(p))).toList();
            return Future.succeededFuture(list);
        } catch (Throwable t) {
            return Future.failedFuture(t);
        }
    }

    @Override
    public Future<List<String>> availablePipeNames() {
        Span span = tracer.span("availablePipeNames");
        try {
            List<String> list = launcher.availablePipes().stream().map(pipe -> pipe.getHeader().getName()).toList();
            span.addEvent("Success", Attributes.of(AttributeKey.stringArrayKey("list"), list));
            return Future.succeededFuture(list);
        } catch (Throwable t) {
            span.recordException(t);
            span.setStatus(StatusCode.ERROR);
            return Future.failedFuture(t);
        } finally {
            span.end();
        }
    }

}
