package io.piveau.scheduling.dashboard;

import io.piveau.scheduling.Constants;
import io.piveau.scheduling.Utils;
import io.piveau.scheduling.launcher.LauncherService;
import io.piveau.scheduling.quartz.QuartzService;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authorization.*;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.thymeleaf.ThymeleafTemplateEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Dashboard {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final QuartzService quartzService;
    private final LauncherService launcherService;

    private final ThymeleafTemplateEngine templateEngine;

    private final String portalUrl;

    public Dashboard(
            Vertx vertx,
            QuartzService quartzService,
            LauncherService launcherService,
            String portalUrl) {
        this.quartzService = quartzService;
        this.launcherService = launcherService;

        this.portalUrl = portalUrl;

        templateEngine = ThymeleafTemplateEngine.create(vertx);
    }

    public void handleGetDashboard(RoutingContext routingContext) {
        Map<String, Object> data = new HashMap<>();

        List<String> permissions = new ArrayList<>();
        List<String> roles = new ArrayList<>();

        if (routingContext.user() != null) {
            User user = routingContext.user();
            data.put("user", user);

            Set<Authorization> authorizations = user.authorizations().get(Constants.AUTH_FILE);
            authorizations.forEach(authorization -> {
                if (authorization instanceof WildcardPermissionBasedAuthorization wba) {
                    permissions.add(wba.getPermission());
                }
                if (authorization instanceof PermissionBasedAuthorization pba) {
                    permissions.add(pba.getPermission());
                }
                if (authorization instanceof RoleBasedAuthorization rba) {
                    roles.add(rba.getRole());
                }
            });
        }

        data.put("zone", ZoneId.systemDefault());
        data.put("permissions", permissions);
        data.put("roles", roles);

        if (!Utils.dashboardLogsUrlTemplate.isBlank()) {
            Instant now = Instant.now();
            data.put("logs", Utils.generateDashboardURL(
                    now.minus(1, ChronoUnit.HOURS),
                    now,
                    ""
            ));
        }

        if (!portalUrl.isBlank()) {
            data.put("portalUrl", portalUrl);
        }

        launcherService.availablePipes()
                .compose(list -> {
                    List<PipeData> pipes = new ArrayList<>();
                    data.put("pipes", pipes);
                    List<Future<JsonArray>> futures = list.stream().map(pipeObject -> {
                        PipeData pipeData = new PipeData(pipeObject);
                        return quartzService.getTriggers(pipeData.getName())
                                .andThen(ar -> {
                                    if (ar.succeeded()) {
                                        pipeData.setTriggers(ar.result());
                                        pipes.add(pipeData);
                                    }
                                });
                    }).toList();
                    return Future.join(futures);
                })
                .compose(v -> launcherService.list(Collections.emptyList()))
                .compose(runArray -> {


                    List<RunData> runs = runArray.stream()
                            .map(JsonObject.class::cast)
                            .map(RunData::new)
                            .sorted(Comparator.comparing(RunData::getStartTime).reversed())
                            .toList();

//                    RunData testRun = new RunData(new JsonObject()
//                            .put("status", "failed")
//                            .put("endTime", Instant.now())
//                            .put("pipeHeader", new JsonObject()
//                                    .put("runId", "govdata~a7082092-ff04-4032-8dbb-cf184882970c")
//                                    .put("name", "testName")
//                                    .put("startTime", Instant.now().minusSeconds(600))
//                            )
//                            .put("reason", "Reason line 1\nReason line 2\nReason line 3")
//                    );
//                    data.put("runs", List.of(testRun));

                    data.put("runs", runs);
                    return templateEngine.render(data, "templates/dashboard.html");
                })
                .onSuccess(buffer -> routingContext.response()
                        .putHeader("content-type", "text/html")
                        .end(buffer))
                .onFailure(routingContext::fail);
    }

}
