package io.piveau.scheduling.dashboard;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class PipeData {

    private JsonObject pipeData;

    private List<TriggerData> triggers = new ArrayList<>();

    public PipeData(JsonObject pipeData) {
        this.pipeData = pipeData;
    }

    public String getId() {
        return pipeData.getJsonObject("header").getString("id");
    }

    public String getName() {
        return pipeData.getJsonObject("header").getString("name");
    }

    public List<TriggerData> getTriggers() {
        return triggers;
    }

    public void setTriggers(List<TriggerData> triggers) {
        this.triggers = triggers;
    }

    public void setTriggers(JsonArray triggers) {
        this.triggers.clear();
        triggers.stream().map(JsonObject.class::cast).map(TriggerData::new).forEach(this.triggers::add);
    }

}
