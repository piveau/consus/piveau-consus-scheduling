package io.piveau.scheduling.dashboard;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class TriggerData {
    private JsonObject triggerData;

    public TriggerData(JsonObject triggerData) {
        this.triggerData = triggerData;
    }

    public String getId() {
        return triggerData.getString("id", "");
    }

    public String getStatus() {
        return triggerData.getString("status", "");
    }

    public Instant getNext() {
        return triggerData.getInstant("next", null);
    }

    public String getFormattedNext() {
        return DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss", Locale.ENGLISH)
                .format(getNext().atZone(ZoneId.systemDefault()));
    }

    public String getValueAsString() {
        if (triggerData.containsKey("interval")) {
            JsonObject interval = triggerData.getJsonObject("interval");
            return "Interval: Every " + interval.getString("value") + " " + interval.getString("unit");
        } else if (triggerData.containsKey("cron")) {
            String cron = triggerData.getString("cron", "");
            return "Cron pattern: " + cron;
        } else if (triggerData.containsKey("specific")) {
            JsonArray specific = triggerData.getJsonArray("specific");
            return "Specific: " + specific.stream().map(String.class::cast).toList();
        } else {
            return "";
        }
    }

}
