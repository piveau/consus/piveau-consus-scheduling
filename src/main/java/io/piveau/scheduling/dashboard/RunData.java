package io.piveau.scheduling.dashboard;

import io.piveau.scheduling.Utils;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class RunData {

    private JsonObject runData;

    public RunData(JsonObject runData) {
        this.runData = runData;
    }

    public String getId() {
        return runData
                .getJsonObject("pipeHeader", new JsonObject())
                .getString("runId", "");
    }

    public String getStatus() {
        return runData.getString("status", "");
    }

    public Instant getStartTime() {
        return runData.getJsonObject("pipeHeader", new JsonObject()).getInstant("startTime", null);
    }

    public String getFormattedStartTime() {
        return DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss", Locale.ENGLISH)
                .format(getStartTime().atZone(ZoneId.systemDefault()));
    }

    public Instant getEndTime() {
        return runData.getInstant("endTime", null);
    }

    public String getFormattedEndTime() {
        Instant endTime = getEndTime();
        if (endTime != null) {
            return DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss", Locale.ENGLISH)
                    .format(endTime.atZone(ZoneId.systemDefault()));
        } else {
            return "";
        }
    }

    public Duration getDuration() {
        Instant startTime = getStartTime();
        Instant endTime = getEndTime();
        if (startTime != null && endTime != null) {
            return Duration.between(startTime, endTime);
        } else {
            return Duration.ZERO;
        }
    }

    public String getFormattedDuration() {
        return DurationFormatUtils.formatDurationWords(getDuration().toMillis(), true, true);
    }

    public String getReason() {
        return runData.getString("reason", "");
    }

    public String getReasonFirstLine() {
        return runData.getString("reason", "").lines().filter(s -> !s.isBlank()).findFirst().orElse("");
    }

    public String getPipeName() {
        return runData.getJsonObject("pipeHeader", new JsonObject()).getString("name", "");
    }

    public String getLogsLink() {
        Instant endTime = getEndTime();
        if (endTime == null) {
            endTime = Instant.now();
        }
        return Utils.generateDashboardURL(runData.getJsonObject("pipeHeader", new JsonObject()).getInstant("startTime", null), endTime, getPipeName());
    }

}
